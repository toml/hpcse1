<!-- File       : faq.md -->
<!-- Created    : Wed Sep 05 2018 07:19:18 PM (+0200) -->
<!-- Author     : Fabian Wermelinger -->
<!-- Description: Frequently Asked Questions -->
<!-- Copyright 2018 ETH Zurich. All Rights Reserved. -->

# Frequently Asked Questions
This is a collection of questions that have been asked on the mailing list and
shall be used a resource of information.

_Please consult this FAQ pool first before you ask a question via the mailing
list [hpcse_hs18@sympa.ethz.ch](mailto:hpcse_hs18@sympa.ethz.ch)_

## Table of Contents
- [Eigen library on Euler](#the-eigen-library-on-the-euler-cluster)
- [Makefile for LaTeX](#makefile-for-latex-projects)
- [Installing Python packages on Euler](#installing-python-packages-on-euler)
- [How to use an interactive node on Euler](#how-to-use-an-interactive-node-on-euler)
- [Roofline model and integer operations](#roofline-model-and-integer-operations)
- [OpenMP critical section](#omp-critical)
- [MPI_Status and MPI_STATUS_IGNORE](#mpi-status)
- [MPI_Bsend vs MPI_Isend](#MPI-Bsend-vs-MPI-Isend)


## The Eigen library on the Euler cluster

The Eigen library is a templated library and does not provide shared object
files but only header files.  In general, if you need additional libraries, you
should use
```
module load <package>
```
You can find the available `packages` by executing
```
module avail
```

In case of the Eigen library, the location of the header files is not added to
your environment when loading the library.  Consequently, the compiler can not
find the headers and will complain at compile time.  You can fix the problem by
specifying the include path manually
```
g++ -I/cluster/apps/eigen/3.2.1/x86_64/gcc_4.8.2/serial/include/eigen3 mycode.cpp
```
If you plan to use the Eigen library more often, you can set the search path of
the `gcc` compiler in your `$HOME/.bashrc` file by adding the line
```
export CPATH=/cluster/apps/eigen/3.2.1/x86_64/gcc_4.8.2/serial/include/eigen3
```
at the end of the file.  This will set the search path for both `gcc` and
`g++`.  But remember, this will always load the Eigen version `3.2.1`.  You
must manually update the path if you want to compile using another version.


##  Makefile for LaTeX projects

You can find an extended version of the `Makefile` for LaTeX documents which
was used as an example in the tutorial slides for the `make` introduction
[here](./latex/Makefile).  It can be used for single- and multi-file LaTeX
projects and assumes you are using BibTeX to build your references.  The
`Makefile` is contained in the lecture Git repo (`faq/latex/Makefile`).  In
order to use the `Makefile`, you must assign the name of your main TeX file to
the variable `MAINFILE` at the beginning of the file.

As an alternative, you might want to have a look at
[latexmk](https://ctan.org/pkg/latexmk?lang=en).

```makefile
# If you want to compile a LaTeX project for which main.tex is the file that
# conatins the \begin{document} ... \end{document} environment, then you need
# to set
#
# MAINFILE = main
#
# in the first line of the Makefile
make               # quick build, cross-references are not correctly resolved
make fast          # same as above
make resolved      # invokes BibTeX and builds the document in 3-stages
make resolved-more # same as above and addtitionally invokes makeindex for index generation
make clean         # clean the working directory
```

## Installing Python packages on Euler

If you need to install a Python package on Euler, you must install it in your
`$HOME` due to write permissions.  For example, you can use the following
command to install the `beautifulsoup` package with python2.7:
```
python -m pip install --user bs4
```
After that you can use `import bs4` in your Python code.


## How to use an interactive node on Euler

Sometimes it is convenient to checkout an interactive node on Euler, rather
than submitting jobs to queue.  Interactive means that you will get back a
shell with the requested resources and you can run your code interactively.
You can request an interactive node on Euler with the following command:
```
bsub -n 24 -W 03:00 -Is /bin/bash
```
As you already know, `bsub` will place your request on the job scheduler queue.
Instead of executing your program, you request a BASH shell in which you will
work once the requested resources have been allocated for you.  The above
example allocates 24 cores for 3 hours.  When you submit this request, you will
have to wait until the job has been allocated for you.  If the cluster is busy,
you might have to wait a while until you receive your interactive shell.

If you want a Haswell node, you can specify it explicitly
```
bsub -n 24 -W 03:00 -R "select[model==XeonE5_2680v3]" -Is /bin/bash
```


## Roofline model and integer operations

Q. Short question: does the counter increment of a for loop count as an operation (addition)? ++k <=> k = k+1
I am wondering this by solving the Question 1 of Set 4, as I actually remember that some weeks ago when we were talking about matrix multiplication those increments weren't taken into account.

A. No, integer operations are not taken into account. Loop index is an integer variable and we count only floating point operations (i.e., floats and doubles).

## OpenMP critical section

Q. I have a question about omp critical. Do the threads synchronize before the omp critical section and then one of them executes the block of code or the first thread to reach the critical section executes it and after that the threads are synchronized?

A. omp critical is a global mutex (lock) - the first thread that takes the lock enters the critical region. As simple as that.
If the critical section is long enough, one thread is already inside and many threads wait for the lock then
as soon as the lock is released, one of the "waiting" threads will get the lock and enter the critical section, and so on.
Therefore, omp critical does not act as an execution barrier.


## MPI_Status and MPI_STATUS_IGNORE

Q. I have a doubt about equating MPI_STATUS_IGNORE to an MPI_Status variable. I have implemented it as :
```
MPI_Status status[4]; //as mentioned in the code.
```
During compilation, I get an error
```
"no match for 'operator=' (operand types are 'MPI_Status {aka ompi_status_public_t}' and
'MPI_Status* {aka ompi_status_public_t*}') status[0] = MPI_STATUS_IGNORE; "
```

A.  *MPI_Status* is a data structure defined in *mpi.h*.
*MPI_Recv* accepts a pointer to a variable of that type. Alternatively, one can use *MPI_STATUS_IGNORE*.
Thus, *MPI_STATUS_IGNORE* is a pointer, defined in *mpi.h*.

For OpenMPI, *MPI_STATUS_IGNORE* is nothing more than the following:
```
#define MPI_STATUS_IGNORE ((MPI_Status *)0)
```
Thus, *''status[0] = MP_STATUS_IGNORE;''* is not a correct statement.

You are free to ignore or remove *''MPI_Status status[4];''* in your solution and directly use *MPI_STATUS_IGNORE* in the *MPI_Recv* calls.

## MPI_Bsend vs MPI_Isend

Q. I have a question about sending messages in MPI.
What exactly is the difference between *MPI_BSend* and the different versions of *MPI_ISend*?
As far as I understand both return before the destination starts receiving.
On slide 30 in *"MPI-Part 2"* it says that the communication is performed asynchronously for *MPI_ISend*,
but I don't understand what that means (i.e. whats the difference between synchronous and asynchronous communication?).

A. Unfortunately MPI is rather confusing with all these sending modes it defines.
There are two main points that specify the behavior of a sending function: <br>
 **a**) *Buffer management*: has the memory (data to be sent) been safely copied internally by the MPI library? <br>
 **b**) *Message transmission*: when the message will be sent by the MPI library to the target rank?

We know that *MPI_Isend* returns immediately to the user. This means that both (**a**) and (**b**) will be performed asynchronously to the user. However, the MPI library may internally use the *Rendezvous* protocol and, thus, wait for the matching *receive* operation from the target rank.

On the other hand, *MPI_Bsend*/ means that the user has provided a memory buffer for internal use by the MPI library. So, the library can copy the application data there and the user can safely modify his data after the call to *MPI_Bsend*. As before, the MPI library will also decide which communication protocol to use. In fact, *MPI_Bsend* performs the buffer management and then calls *MPI_Isend* (mpich-3.3a2/src/mpi/pt2pt/bsend.c)!

A simple and safe approach is to avoid initially any assumptions about buffer management and be sure that what you sent is not modified immediately afterwards.
In fact, this is the most common case for any applications (e.g. MPI diffusion).
